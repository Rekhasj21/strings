function string3(date) {

    const dateParts = date.split('/');
    let month = parseInt(dateParts[0]);
    let day = parseInt(dateParts[1]);
    let year = parseInt(dateParts[2]);
    if (month > 12) {
        temp = month;
        month = day;
        day = temp;
    }
    const newDate = `${day}/${month}/${year}`;
    return newDate;
}

module.exports = string3