function string4(obj){
    let fullName = "";
      
    fullName += obj.first_name.charAt(0).toUpperCase() + obj.first_name.slice(1).toLowerCase();
      
    if (obj.middle_name) {
        fullName += " " + obj.middle_name.charAt(0).toUpperCase() + obj.middle_name.slice(1).toLowerCase();
    }
    
    fullName += " " + obj.last_name.charAt(0).toUpperCase() + obj.last_name.slice(1).toLowerCase();
      
    return fullName;
    }
    
module.exports = string4