function string1(numberArray) {
  let result = [];

  for (let i = 0; i < numberArray.length; i++) {
    let numString = numberArray[i].replace(/[^0-9.-]+/g, "");
    let num = parseFloat(numString);

    if (isNaN(num)) {
      result.push(0);
    } else {
      result.push(num);
    }
  }
  return result
}

module.exports = string1