function string2(ipAddress){
       const numericParts = ipAddress.split('.');
        if (numericParts.length !== 4) {
          return [];
        }
        for (let i = 0; i < numericParts.length; i++) {
          const num = parseInt(numericParts[i]);
          if (isNaN(num) || num < 0 || num > 255) {
            return [];
          }
          numericParts[i] = num;
        }
        return numericParts;
      }

module.exports = string2